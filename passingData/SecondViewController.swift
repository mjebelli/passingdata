//
//  SecondViewController.swift
//  passingData
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۲/۱۵.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

import UIKit
import MessageUI



class SecondViewController: UIViewController, MFMailComposeViewControllerDelegate {

  //  @IBOutlet weak var viaSegueLabel: UILabel!
    
    //var viaSegue = ""
    
    
    //var describe = [String]();
    
    
    
    
    
    //let array = ["First","Second","Third"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

                // Do any additional setup after loading the view.
        
        //describe = ["Iran zamin eshghast", "England ghavi boode"]

      //  self.addRightNavItemOnView()
    }
    
    
    var myStringDate: String = ""
    
    
    
    @IBOutlet  weak var Location: UITextField! = UITextField()
    @IBOutlet  weak var Price: UITextField! = UITextField()
    @IBOutlet  weak var myDatePicker: UIDatePicker! = UIDatePicker()
    
    
    
    
    
    
    var enteredData = [String: String]()
    


    
    @IBAction func saveButton(sender: AnyObject) {
    }
        
    
    @IBAction func myDatePickerAction(sender: AnyObject) {
        
        let formatMyDate = NSDateFormatter()
        formatMyDate.dateFormat = "MMM-yy"
        myStringDate = formatMyDate.stringFromDate(myDatePicker.date)
        
    }
    
    
    

    
   //////////////////////////////////////////////////////////
    @IBAction func btnTouched(sender: UIButton) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }

        
        /*
        let mailComposeController = MFMailComposeViewController()
        mailComposeController.mailComposeDelegate = self;
        mailComposeController.setToRecipients(["mj.jebelli@gmail.com"])
        mailComposeController.setSubject("Recent Spent")
        mailComposeController.setMessageBody("Salam dash khoobi , man Nokaaaretam dooooaash!!", isHTML: false)
        
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeController, animated: true, completion: { () -> Void in
                
            })
        }

        */
    }
    
  
    
    
    
    
    

    /*
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return array.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return array[row]
    }
    */
    
    
    

    
    
    func configuredMailComposeViewController()-> MFMailComposeViewController {
        let mailComposerMC = MFMailComposeViewController()
        mailComposerMC.mailComposeDelegate = self
        mailComposerMC.setToRecipients(["mj.jebelli@gmail.com"])
        mailComposerMC.setSubject("Spend Money Detail")
        mailComposerMC.setMessageBody("How you dare to spend this much!", isHTML: false)
        
        return mailComposerMC
    }

    
    
    func showSendMailErrorAlert() {
        let alertController = UIAlertController(title: "Could Not Send Mail", message: "Mail Did Not Send, Please try again!", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            
        }
    }
    
    
    
    
    func mailComposeController(controller: MFMailComposeViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SendDataSegue" {
            
            enteredData = ["spent" : self.Price.text!, "location" : self.Location.text!, "date" :myStringDate]
            Singleton.sharedInstance().appendDictionary(enteredData)
            
            
            
        }
    }

    
    
    /*
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true) { () -> Void in
            
            }
    }
    
    
    
    
    func printDate (date :NSDate) {
        let dateformatter = NSDateFormatter();
        dateformatter.dateStyle = NSDateFormatterStyle.MediumStyle;
        dateformatter.timeStyle = NSDateFormatterStyle.MediumStyle;
        NSLog("%@", dateformatter.stringFromDate(date))
    }
    
    */
    
    
    
        
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
