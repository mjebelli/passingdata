//
//  singleton.swift
//  passingData
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۲/۲۸.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

import Foundation



class Singleton {
    static var instance: Singleton!
    var Data = [[String: String]]()
    
    class func sharedInstance() -> Singleton {
        self.instance = (self.instance ?? Singleton())
        return self.instance
    }
    
    
    init() {
    }
    func appendDictionary(dic: [String: String]) {
        self.Data.append(dic)
    }
}